# README

## screenshots

![screenshot](notes/nested-01a.gif)

![screenshot](notes/nested-01b.gif)

## short explanation

same demo app as [nested-01](https://gitlab.com/computalya/nested-01/)

difference is the use of **shallow** in **nested resources**

#### update `config/routes.rb`

> config/routes.rb

```ruby
  resources :projects do
    resources :tasks, shallow: true
  end
```

#### update paths/links in views

| link    | before                | after                          |
|---------|-----------------------|--------------------------------|
| new     | `new_task_path`       | `new_project_task_path`        |
| back    | `project_tasks_url`   | `project_tasks_url(@project)`  |

#### update form_with formula to add second model/url

```erb
<%= form_with(model: task,
              url: (@task.new_record? ? [@project, task] : task_path),
              local: true) do |form| %>
```

#### update controller to set up instance variable(`@project`) and path

```ruby
class TasksController < ApplicationController
  before_action :set_project
  # ...
  private

    def set_project
      # if params[:project_id].present?
      #   @project = Project.find(params[:project_id])
      # else
      #   @project = Task.find(params[:id]).project
      # end
      @project = params.dig(:project_id) ? Project.find(params[:project_id]) : Task.find(params[:id]).project
    end
```

replace everywhere

| before              | after                            |
|---------------------|----------------------------------|
| `Task.`             | `@project.tasks.`                |

replace in `destroy` section

| before              | after                            |
|---------------------|----------------------------------|
| `redirect_to project_tasks_url` | `redirect_to project_tasks_url(@project)` |


## setup demoapp

install a new using application template [jumpstart](https://gitlab.com/computalya/jumpstart)

Note: _simple\_form_ is not needed!

```bash
rails new nested-shallow -m $template -B -T
cd nested-shallow
bundle install --local
```

## scaffold _Project_ and _Task_ 

```bash
rails g scaffold Project name
rails g scaffold Task name project:references
rails db:migrate
```

`task.rb` should look as below

>  app/models/task.rb

```ruby
class Task < ApplicationRecord
  belongs_to :project
end
```

update `project.rb`

>  app/models/project.rb

```ruby
class Project < ApplicationRecord
  has_many :tasks, dependent: :destroy
end
```

seed database

> db/seeds.rb

```ruby
project1 = Project.create! name: 'Star Wars Project'
project2 = Project.create! name: 'Project Dagobah'

# create documents for each project
task1 = project1.tasks.create! name: 'task1 of Star Wars'
task2 = project1.tasks.create! name: 'task2 of Star Wars'

task3 = project2.tasks.create! name: 'task3 of Dagobah'
task4 = project2.tasks.create! name: 'task4 of Dagobah'
```

reset and seed database with new data

```bash
rails db:reset
```

## add nested resources 

Tasks should be attached to a project(nested)

check current routes

```bash
rails routes

             Prefix Verb   URI Pattern                                         Controller#Action
              tasks GET    /tasks(.:format)                                    tasks#index
           new_task GET    /tasks/new(.:format)                                tasks#new
          edit_task GET    /tasks/:id/edit(.:format)                           tasks#edit
```

update `routes.rb`

> config/routes.rb

```ruby
  resources :projects do
    resources :tasks
  end
```

check again routes

```bash
rails routes

             Prefix Verb   URI Pattern                                         Controller#Action
      project_tasks GET    /projects/:project_id/tasks(.:format)               tasks#index
   new_project_task GET    /projects/:project_id/tasks/new(.:format)           tasks#new
  edit_project_task GET    /projects/:project_id/tasks/:id/edit(.:format)      tasks#edit
```

now `tasks` are nested under `projects`

## add shallow to nested resources 

> config/routes.rb

```ruby
  resources :projects do
    resources :tasks, shallow: true
  end
```

now check routes again. as you can see some of the url's has been changed like `edit_task`

```bash
rails routes

             Prefix Verb   URI Pattern                                         Controller#Action
      project_tasks GET    /projects/:project_id/tasks(.:format)               tasks#index
   new_project_task GET    /projects/:project_id/tasks/new(.:format)           tasks#new
          edit_task GET    /tasks/:id/edit(.:format)                           tasks#edit
```

shallo is useful, if you like the sorten url's/paths and shorten your code

## add link to navigation

> app/views/layouts/\_navigation.html.erb

```html
  <ul class="menu align-right">
    <li>
      <%= link_to 'Projects', projects_url  %>
    </li>
```

#### set `@project` for tasks

to use `@project` instead of `task.project`

> app/controllers/tasks_controller.rb

```ruby
class TasksController < ApplicationController
  before_action :set_project
  # ...
  private
    def set_project
      if params[:project_id].present?
        @project = Project.find(params[:project_id])
      else
        @project = Task.find(params[:id]).project
      end
    end
```


## list tasks of a project

> app/views/projects/show.html.erb

add link to _Tasks_ 

`link_to 'Tasks', project_tasks_url(@project)` 

```erb
<p id="notice"><%= notice %></p>

<p>
  <strong>Name:</strong>
  <%= @project.name %>
</p>

<h5>
  <%= link_to 'Tasks', project_tasks_url(@project) %>
</h5>

<%= link_to 'Edit', edit_project_path(@project) %> |
<%= link_to 'Back', projects_path %>
```

## update tasks/index

there are view changes to the in this view

#### the `New Task` link has to be changed


| before       | after                 |
|--------------|-----------------------|
|new_task_path | new_project_task_path |


#### show the name of the project


| before       | after             |
|--------------|-------------------|
| task.project | @project.name     |


#### this is how it looks at the end

add link to new project task and project name

> app/views/tasks/index.html.erb

```erb
<td><%= @project.name %></td>

<%= link_to 'New Task', new_project_task_path %>
```

#### update tasks views

> app/views/tasks/show.html.erb
- add project name
- replace `Edit` and `Back` links

```erb
<p id="notice"><%= notice %></p>

<p>
  <strong>Name:</strong>
  <%= @task.name %>
</p>

<p>
  <strong>Project:</strong>
  <%= @project.name %>
</p>

<%= link_to 'Edit', edit_task_path(@task) %> |
<%= link_to 'Back', project_tasks_url(@project) %>
```

> app/views/tasks/\_form.html.erb

- to use nested resources with `form_with` we have to give the url as a parameter
- additionally we have to check whether it is a new record or not to give correct url. Otherwise we will get a routing error

before

```erb
<%= form_with(model: task, local: true) do |form| %>
```

after

```erb
<%= form_with(model: task,
              url: (@task.new_record? ? [@project, task] : task_path),
              local: true) do |form| %>
```

hide `project.id`

> app/views/tasks/\_form.html.erb

```erb
<%= form_with(model: task,
              url: (@task.new_record? ? [@project, task] : task_path),
              local: true) do |form| %>
  <% if task.errors.any? %>
    <div id="error_explanation">
      <h2><%= pluralize(task.errors.count, "error") %> prohibited this task from being saved:</h2>

      <ul>
      <% task.errors.full_messages.each do |message| %>
        <li><%= message %></li>
      <% end %>
      </ul>
    </div>
  <% end %>

  <%= form.label :name %>
  <%= form.text_field :name %>

  <%= form.hidden_field :project_id %>

  <%= form.submit %>
<% end %>
```

and add correct `Back` link in

> app/views/tasks/new.html.erb

```erb
<%= link_to 'Back', project_tasks_url %>
```

> app/views/tasks/edit.html.erb

- update links

```erb
<h1>Editing Task</h1>

<%= render 'form', task: @task %>

<%= link_to 'Show', @task %> |
<%= link_to 'Back', project_tasks_url(@project) %>
```


#### final changes in the `tasks_controller.rb`

| before                                               | after                                     |
|------------------------------------------------------|-------------------------------------------|
| `Task.`                                              | `@project.tasks.`                         |
| `redirect_to project_tasks_url` in `destroy` section | `redirect_to project_tasks_url(@project)` |

at the end it looks like this

> app/controllers/tasks_controller.rb

```ruby
class TasksController < ApplicationController
  before_action :set_project
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  # GET /tasks
  # GET /tasks.json
  def index
    @tasks = @project.tasks.all
  end

  # GET /tasks/1
  # GET /tasks/1.json
  def show
  end

  # GET /tasks/new
  def new
    @task = @project.tasks.new
  end

  # GET /tasks/1/edit
  def edit
  end

  # POST /tasks
  # POST /tasks.json
  def create
    @task = @project.tasks.new(task_params)

    respond_to do |format|
      if @task.save
        format.html { redirect_to project_tasks_url, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tasks/1
  # PATCH/PUT /tasks/1.json
  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to @task, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task }
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.json
  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to project_tasks_url(@project), notice: 'Task was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      # if params[:project_id].present?
      #   @project = Project.find(params[:project_id])
      # else
      #   @project = Task.find(params[:id]).project
      # end
      @project = params.dig(:project_id) ? Project.find(params[:project_id]) : Task.find(params[:id]).project
    end

    def set_task
      # @project = Task.find(params[:id]).project unless @project.present?
      @task = @project.tasks.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:name, :project_id)
    end
end
```

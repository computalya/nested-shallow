class SecurePagesController < ApplicationController
  before_action :authenticate_user!
  after_action  :verify_authorized

  def user_dashboard
    authorize :secure_page, :user_dashboard?
  end

  def admin_dashboard
    authorize :secure_page, :admin_dashboard?
  end

  def common_dashboard
    authorize :secure_page, :common_dashboard?
  end
end


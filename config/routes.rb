Rails.application.routes.draw do
  resources :projects do
    resources :tasks, shallow: true
  end

  root 'home#index'
  get 'home/index'
  get 'secure_pages/common_dashboard'
  get 'secure_pages/admin_dashboard'
  get 'secure_pages/user_dashboard'
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
